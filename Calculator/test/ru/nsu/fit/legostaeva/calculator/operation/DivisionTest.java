package ru.nsu.fit.legostaeva.calculator.operation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;
import ru.nsu.fit.legostaeva.calculator.exception.maths.DivisionByZeroException;

import java.util.ArrayList;
import java.util.List;

class DivisionTest {
    private Division division = new Division();
    private Context context = new Context();
    private List<String> params = new ArrayList<>();

    @Test
    void testNormalDivision(){
        final float firstValue = 4;
        final float secondValue = 2;
        context.pushOnStack(firstValue);
        context.pushOnStack(secondValue);
        try {
            division.evaluate(params, context);
        } catch (StackIsEmptyException | TooManyArgumentsException | DivisionByZeroException e) {
            e.printStackTrace();
        }
        try {
            Assertions.assertEquals(firstValue / secondValue, context.popFromStack(), 0.01);
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testDivisionByZero(){
        final float firstValue = 4;
        final float secondValue = 0;
        context.pushOnStack(firstValue);
        context.pushOnStack(secondValue);
        Assertions.assertThrows(DivisionByZeroException.class, () -> division.evaluate(params, context));
    }

}