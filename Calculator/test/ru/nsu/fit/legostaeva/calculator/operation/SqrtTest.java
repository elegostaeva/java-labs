package ru.nsu.fit.legostaeva.calculator.operation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;
import ru.nsu.fit.legostaeva.calculator.exception.maths.NegativeSqrtException;

import java.util.ArrayList;
import java.util.List;

class SqrtTest {
    private Sqrt sqrt = new Sqrt();
    private Context context = new Context();
    private List<String> params = new ArrayList<>();

    @Test
    void testNormalSqrt(){
        final float value = 4;
        context.pushOnStack(value);
        try {
            sqrt.evaluate(params, context);
        } catch (StackIsEmptyException | TooManyArgumentsException | NegativeSqrtException e) {
            e.printStackTrace();
        }
        try {
            Assertions.assertEquals(Math.sqrt(value), context.popFromStack(), 0.01);
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSqrtByZero(){
        final float value = -4;
        context.pushOnStack(value);
        Assertions.assertThrows(NegativeSqrtException.class, () -> sqrt.evaluate(params, context));
    }

}