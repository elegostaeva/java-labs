package ru.nsu.fit.legostaeva.calculator.operation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;

import java.util.ArrayList;
import java.util.List;

class MultiplicationTest {
    private Multiplication multiplication = new Multiplication();
    private Context context = new Context();
    private List<String> params = new ArrayList<>();

    @Test
    void testMultiplication(){
        final float firstValue = 2;
        final float secondValue = 4;
        context.pushOnStack(firstValue);
        context.pushOnStack(secondValue);
        try {
            multiplication.evaluate(params, context);
        } catch (StackIsEmptyException | TooManyArgumentsException e) {
            e.printStackTrace();
        }
        try {
            Assertions.assertEquals(firstValue * secondValue, context.popFromStack(), 0.01);
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }

}