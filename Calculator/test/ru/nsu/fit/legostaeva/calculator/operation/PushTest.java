package ru.nsu.fit.legostaeva.calculator.operation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.NotEnoughArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;
import ru.nsu.fit.legostaeva.calculator.exception.context.VariableNotFoundException;

import java.util.ArrayList;
import java.util.List;

class PushTest {
    private Push push = new Push();
    private Context context = new Context();
    private List<String> params = new ArrayList<>();

    @Test
    void testPush(){
        final float value = 3;
        params.add(Float.toString(value));
        try {
            push.evaluate(params, context);
        } catch (NotEnoughArgumentsException | TooManyArgumentsException | VariableNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Assertions.assertEquals(value, context.popFromStack());
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }

}