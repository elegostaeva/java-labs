package ru.nsu.fit.legostaeva.calculator.operation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;

import java.util.ArrayList;
import java.util.List;

class PlusTest {
    @Test
    void testPlus(){
        Plus plus = new Plus();
        Context context = new Context();
        List<String> params = new ArrayList<>();
        final float firstValue = 3;
        final float secondValue = 4;
        context.pushOnStack(firstValue);
        context.pushOnStack(secondValue);
        try {
            plus.evaluate(params, context);
        } catch (StackIsEmptyException | TooManyArgumentsException e) {
            e.printStackTrace();
        }
        try {
            Assertions.assertEquals(firstValue + secondValue, context.popFromStack(), 0.01);
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }

}