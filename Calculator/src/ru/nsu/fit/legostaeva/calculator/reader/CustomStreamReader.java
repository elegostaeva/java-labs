package ru.nsu.fit.legostaeva.calculator.reader;

import java.io.InputStream;
import java.util.Scanner;

public class CustomStreamReader implements CustomReader {

    private Scanner reader;

    public CustomStreamReader(InputStream stream){
        this.reader = new Scanner(stream);
    }

    public String nextLine(){
        return reader.nextLine();
    }

    public boolean hasNextLine() {
        return reader.hasNextLine();
    }
}
