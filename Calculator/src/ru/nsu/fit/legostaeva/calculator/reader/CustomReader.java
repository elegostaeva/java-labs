package ru.nsu.fit.legostaeva.calculator.reader;

import java.io.IOException;

public interface CustomReader {
    String nextLine() throws IOException;
    boolean hasNextLine();
}
