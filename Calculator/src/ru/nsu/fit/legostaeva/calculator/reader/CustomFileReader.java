package ru.nsu.fit.legostaeva.calculator.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class CustomFileReader implements CustomReader{

    private BufferedReader reader;
    private String curLine;

    public CustomFileReader(FileReader reader) throws IOException {
        this.reader = new BufferedReader(reader);
        curLine = this.reader.readLine();
    }

    public String nextLine() throws IOException {
        String tmp = curLine;
        curLine = reader.readLine();
        return tmp;
    }

    public boolean hasNextLine() {
        return (curLine != null);
    }
}
