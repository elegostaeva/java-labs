package ru.nsu.fit.legostaeva.calculator;

import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;
import ru.nsu.fit.legostaeva.calculator.exception.context.VariableNotFoundException;

import java.util.*;

public class Context {
    private Deque<Float> valuesStack = new ArrayDeque<>();
    private Map<String, Float> variablesValue = new HashMap<>();

    public void pushOnStack(float value){
        valuesStack.addFirst(value);
    }

    public float popFromStack() throws StackIsEmptyException {
        if (valuesStack.isEmpty()){
            throw new StackIsEmptyException("Stack is empty");
        }
        return valuesStack.pop();
    }

    public void setValueOfVariable(String variable, float value){
        variablesValue.put(variable, value);
    }

    public float getValueOfVariable(String variable) throws VariableNotFoundException {
        if (variablesValue.containsKey(variable)) {
            return variablesValue.get(variable);
        }
        else {
            throw new VariableNotFoundException("Variable not found");
        }
    }
}
