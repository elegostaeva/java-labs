package ru.nsu.fit.legostaeva.calculator;

import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.WrongCommandNameException;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.WrongNumberOfArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.ContextException;
import ru.nsu.fit.legostaeva.calculator.exception.maths.MathsOperationException;
import ru.nsu.fit.legostaeva.calculator.operation.Factory;
import ru.nsu.fit.legostaeva.calculator.operation.Operation;
import ru.nsu.fit.legostaeva.calculator.reader.CustomReader;

import java.io.IOException;
import java.util.ArrayList;


class Calculator {
    void calculate(CustomReader reader) {
        Context context = new Context();
        Parser parser = new Parser();
        Factory factory = Factory.getInstance();
        while (reader.hasNextLine()) {
            try {
                String curLine = reader.nextLine();
                ArrayList<String> params = parser.getParams(curLine);
                if (params.size() == 0) {
                    continue;
                }
                Operation operation = factory.getOperationByName(params.get(0));
                params.remove(0);
                operation.evaluate(params, context);
            } catch (IOException e) {
                e.printStackTrace();
            } /*catch (MathsOperationException | WrongNumberOfArgumentsException | ContextException | WrongCommandNameException e) {
                System.out.println(e.getMessage());
            }*/ catch (CalculatorException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
