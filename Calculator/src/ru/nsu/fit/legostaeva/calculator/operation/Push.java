package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.NotEnoughArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.VariableNotFoundException;

import java.util.List;

public class Push implements Operation{
    public void evaluate(List<String> params, Context context) throws NotEnoughArgumentsException, TooManyArgumentsException, VariableNotFoundException {
        if (params.size() > 1){
            throw new TooManyArgumentsException("Too many arguments");
        }
        else if(params.size() < 1){
            throw new NotEnoughArgumentsException("Not enough arguments");
        }
        try{
            float value = Float.parseFloat(params.get(0));
            context.pushOnStack(value);
        }
        catch (NumberFormatException e){
            float value = context.getValueOfVariable(params.get(0));
            context.pushOnStack(value);
        }
    }
}
