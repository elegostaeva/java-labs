package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.exception.arguments.WrongCommandNameException;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Factory {
    private static final Factory ourInstance = new Factory();
    private Map<String, Operation> allOperations = new HashMap<>();

    public static Factory getInstance() {
        return ourInstance;
    }



    private Factory() {
        Properties prop = new Properties();
        try {
            prop.load(Factory.class.getClassLoader().getResourceAsStream("config.properties"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        for(Object key : prop.keySet()){
            try {
                allOperations.put(key.toString(), (Operation) Class.forName(prop.getProperty((String) key)).newInstance());
            } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public Operation getOperationByName(String operationName) throws WrongCommandNameException {
        Operation result = allOperations.get(operationName);
        if (result == null){
            throw new WrongCommandNameException("Wrong command name");
        }
        return result;
    }
}
