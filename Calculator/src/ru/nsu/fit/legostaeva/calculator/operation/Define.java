package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.NotEnoughArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;

import java.util.List;

public class Define implements Operation {
    public void evaluate(List<String> params, Context context) throws NotEnoughArgumentsException, TooManyArgumentsException {
        if (params.size() > 2){
            throw new TooManyArgumentsException("Too many arguments");
        }
        else if(params.size() < 2){
            throw new NotEnoughArgumentsException("Not enough arguments");
        }
        float value = Float.parseFloat(params.get(1)); //обрабатывает калькулятор
        String variable = params.get(0);
        context.setValueOfVariable(variable, value);
    }
}
