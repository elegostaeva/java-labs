package ru.nsu.fit.legostaeva.calculator.operation;


import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;

import java.util.List;

public interface Operation {
    void evaluate(List<String> params, Context context) throws CalculatorException;
}
