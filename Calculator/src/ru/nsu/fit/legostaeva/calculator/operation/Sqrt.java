package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.maths.NegativeSqrtException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;

import java.util.List;

public class Sqrt implements Operation {
    public void evaluate(List<String> params, Context context) throws NegativeSqrtException, StackIsEmptyException, TooManyArgumentsException {
        if (params.size() != 0){
            throw new TooManyArgumentsException("Too many arguments");
        }
        float value = context.popFromStack();
        if (value < 0){
            throw new NegativeSqrtException("Cannot take root of a negative number");
        }
        float result = (float)Math.sqrt(value);
        context.pushOnStack(result);
    }
}
