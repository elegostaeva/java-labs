package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;

import java.util.List;

public class Pop implements Operation {
    public void evaluate(List<String> params, Context context) throws StackIsEmptyException, TooManyArgumentsException {
        if (params.size() != 0){
            throw new TooManyArgumentsException("Too many arguments");
        }
        context.popFromStack();
    }
}
