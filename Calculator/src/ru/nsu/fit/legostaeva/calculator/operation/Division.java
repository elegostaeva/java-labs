package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;
import ru.nsu.fit.legostaeva.calculator.exception.maths.DivisionByZeroException;

import java.util.List;

public class Division implements Operation {
    public void evaluate(List<String> params, Context context) throws DivisionByZeroException, StackIsEmptyException, TooManyArgumentsException {
        if (params.size() != 0){
            throw new TooManyArgumentsException("Too many arguments");
        }
        float first = context.popFromStack();
        if (first == 0){
            throw new DivisionByZeroException("Cannot be divided by zero");
        }
        float second = context.popFromStack();
        float result = second/first;
        context.pushOnStack(result);
    }
}
