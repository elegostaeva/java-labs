package ru.nsu.fit.legostaeva.calculator.operation;

import ru.nsu.fit.legostaeva.calculator.Context;
import ru.nsu.fit.legostaeva.calculator.exception.arguments.TooManyArgumentsException;
import ru.nsu.fit.legostaeva.calculator.exception.context.StackIsEmptyException;

import java.util.List;

public class Multiplication implements Operation {
    public void evaluate(List<String> params, Context context) throws StackIsEmptyException, TooManyArgumentsException {
        if (params.size() != 0){
            throw new TooManyArgumentsException("Too many arguments");
        }
        float first = context.popFromStack();
        float second = context.popFromStack();
        float result = first * second;
        context.pushOnStack(result);
    }
}
