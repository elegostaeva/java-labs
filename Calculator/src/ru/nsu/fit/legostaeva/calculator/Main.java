package ru.nsu.fit.legostaeva.calculator;

import ru.nsu.fit.legostaeva.calculator.reader.CustomFileReader;
import ru.nsu.fit.legostaeva.calculator.reader.CustomReader;
import ru.nsu.fit.legostaeva.calculator.reader.CustomStreamReader;

import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        CustomReader reader;
        if ((args.length == 1) && !"-h".equals(args[0])){
            try(FileReader reader1 = new FileReader(args[0])){ //try with resources закрывает то, что в (...)
                reader = new CustomFileReader(reader1);
                executeCalc(reader);
            }
        }
        else {
            reader = new CustomStreamReader(System.in);
            executeCalc(reader);
        }
    }

    private static void executeCalc(CustomReader reader) {
        Calculator calculator = new Calculator();
        calculator.calculate(reader);
    }
}
