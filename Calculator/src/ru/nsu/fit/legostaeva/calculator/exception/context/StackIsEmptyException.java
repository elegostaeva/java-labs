package ru.nsu.fit.legostaeva.calculator.exception.context;

public class StackIsEmptyException extends ContextException {
    public StackIsEmptyException(String message) {
        super(message);
    }
}
