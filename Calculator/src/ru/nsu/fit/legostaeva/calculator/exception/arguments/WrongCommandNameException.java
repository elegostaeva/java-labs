package ru.nsu.fit.legostaeva.calculator.exception.arguments;

import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;

public class WrongCommandNameException extends CalculatorException {
    public WrongCommandNameException(String message) {
        super(message);
    }
}
