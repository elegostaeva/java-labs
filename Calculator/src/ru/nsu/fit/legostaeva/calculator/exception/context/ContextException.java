package ru.nsu.fit.legostaeva.calculator.exception.context;

import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;

public class ContextException extends CalculatorException {
    ContextException(String message) {
        super(message);
    }
}
