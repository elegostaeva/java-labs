package ru.nsu.fit.legostaeva.calculator.exception.maths;

public class DivisionByZeroException extends MathsOperationException {

    public DivisionByZeroException(String message) {
        //System.out.println("Cannot be divided by zero");
        super(message);
    }

}
