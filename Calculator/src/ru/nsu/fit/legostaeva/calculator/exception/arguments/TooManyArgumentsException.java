package ru.nsu.fit.legostaeva.calculator.exception.arguments;

public class TooManyArgumentsException extends WrongNumberOfArgumentsException {
    public TooManyArgumentsException(String message) {
        super(message);
    }
}
