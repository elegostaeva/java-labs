package ru.nsu.fit.legostaeva.calculator.exception.context;

public class VariableNotFoundException extends ContextException {
    public VariableNotFoundException(String message) {
        super(message);
    }
}
