package ru.nsu.fit.legostaeva.calculator.exception.maths;

public class NegativeSqrtException extends MathsOperationException {
    public NegativeSqrtException(String message) {
        super(message);
    }
}
