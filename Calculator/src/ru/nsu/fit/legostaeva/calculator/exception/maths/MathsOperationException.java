package ru.nsu.fit.legostaeva.calculator.exception.maths;

import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;

public class MathsOperationException extends CalculatorException {
    MathsOperationException(String message) {
        super(message);
    }
}
