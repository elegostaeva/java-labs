package ru.nsu.fit.legostaeva.calculator.exception.arguments;

import ru.nsu.fit.legostaeva.calculator.exception.CalculatorException;

public class WrongNumberOfArgumentsException extends CalculatorException {
    WrongNumberOfArgumentsException(String message) {
        super(message);
    }
}
