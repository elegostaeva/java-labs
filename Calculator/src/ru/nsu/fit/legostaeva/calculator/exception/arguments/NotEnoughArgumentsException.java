package ru.nsu.fit.legostaeva.calculator.exception.arguments;

public class NotEnoughArgumentsException extends WrongNumberOfArgumentsException {
    public NotEnoughArgumentsException(String message) {
        super(message);
    }
}
