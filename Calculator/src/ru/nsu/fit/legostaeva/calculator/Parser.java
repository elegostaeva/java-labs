package ru.nsu.fit.legostaeva.calculator;

import java.util.ArrayList;
import java.util.Collections;

class Parser {
    ArrayList<String> getParams(String curString){
        ArrayList<String> result = new ArrayList<>();

        if (curString.isEmpty() || '#' == (curString.charAt(0))){
            return result;
        }

        Collections.addAll(result, curString.split(" +"));

        return result;
    }
}