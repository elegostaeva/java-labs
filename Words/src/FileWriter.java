import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public interface FileWriter {
    void write(PrintWriter writer, List<Map.Entry<String, Integer>> list, int frequency);
}
