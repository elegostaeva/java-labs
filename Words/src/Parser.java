import java.io.IOException;

public interface Parser {
    String getNextLexeme() throws IOException;
}
