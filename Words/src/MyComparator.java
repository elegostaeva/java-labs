import java.util.Comparator;
import java.util.Map;

public class MyComparator implements Comparator<Map.Entry<String, Integer>> {

    @Override
    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
        int rs = (o2.getValue().compareTo(o1.getValue()));
        return rs == 0 ? o1.getKey().compareTo(o2.getKey()): rs;//(o2.getValue() - o1.getValue());
    }
}
