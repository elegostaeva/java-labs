import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class CSVWriter implements FileWriter{
    public void write(PrintWriter writer, List<Map.Entry<String, Integer>> list, int frequency) {
        writer.write("Word; Frequency; %\n");
        for (Map.Entry<String, Integer> curColumn : list){
            writer.append(curColumn.getKey())
                    .append(';')
                    .append(curColumn.getValue().toString())
                    .append(';').append("").append(String.valueOf((curColumn.getValue() *  100.f) / frequency))
                    .append(';')
                    .append('\n');
            }
    }
}
