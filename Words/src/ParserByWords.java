import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;

class ParserByWords implements Parser {
    private Reader reader;
    private boolean hasNoMoreWords = false;

    public String getNextLexeme() throws IOException {
        int cur;
        if (hasNoMoreWords) {
            return null;
        }
        cur = reader.read();
        while (!Character.isLetterOrDigit(cur)) {
            cur = reader.read();
            if (cur == -1) {
                hasNoMoreWords = true;
                return null;
            }
        }
        StringBuilder word = new StringBuilder();
        word.append((char) cur);
        while (Character.isLetterOrDigit(cur = reader.read())) {
            word.append((char) cur);
        }
        if (cur == -1) {
            hasNoMoreWords = true;
        }
        return word.toString();
    }

    ParserByWords(Reader reader) {
        this.reader = reader;
    }
}

