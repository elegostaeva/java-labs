import java.io.*;
import java.util.*;

public class MainClass {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        int frequency = 0;
        try (Reader reader = new FileReader(args[0])) {
            Parser parser = new ParserByWords(reader);
            String curWord;
            while((curWord = parser.getNextLexeme()) != null){
                map.put(curWord, map.getOrDefault(curWord, 0) + 1);
                frequency++;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>(map.entrySet());
        MyComparator comparator = new MyComparator();
        list.sort(comparator);
        //System.out.println(list);
        try (PrintWriter writer = new PrintWriter(args[1])) {
            FileWriter outputWriter = new CSVWriter();
            outputWriter.write(writer, list, frequency);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}