package ru.nsu.ccfit.legostaeva.minesweeper;

public class Cell {
    public enum CellState{
        OPENED, CLOSED, FLAG, QUESTION
    }
    private boolean mined;
    private CellState currentState = CellState.CLOSED;
    private int numberOfMinesAround;

    public boolean isMined() {
        return mined;
    }

    public void setMined(boolean mined) {
        this.mined = mined;
    }

    public CellState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(CellState currentState) {
        this.currentState = currentState;
    }

    public int getNumberOfMinesAround() {
        return numberOfMinesAround;
    }

    public void setNumberOfMinesAround(int numberOfMinesAround) {
        this.numberOfMinesAround = numberOfMinesAround;
    }

    public void increaseNumberOfMinesAround(){ this.numberOfMinesAround++; }
}
