package ru.nsu.ccfit.legostaeva.minesweeper.controller;

import ru.nsu.ccfit.legostaeva.minesweeper.Cell;
import ru.nsu.ccfit.legostaeva.minesweeper.Click;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments.TooBigValueException;
import ru.nsu.ccfit.legostaeva.minesweeper.model.Model;
import ru.nsu.ccfit.legostaeva.minesweeper.view.View;
import ru.nsu.ccfit.legostaeva.minesweeper.view.gui.GameLevel;

import java.util.Timer;
import java.util.TimerTask;


public class Controller {
    private Model model;
    private View view;
    private Timer timer = new Timer();
    private boolean isGameOver = false;
    private boolean isGameWon = false;
    private boolean isFirstClick = true;
    public boolean fieldIsInitialized = true;
    private boolean gameRestarted = false;
    private boolean gameWasWon = false;
    private GameLevel gameLevel = GameLevel.BEGINNER;

    public GameLevel getGameLevel() {
        return gameLevel;
    }

    public Controller(int height, int width, int amountOfMines, View view) {
        /*try {
            model = new Model(height, width, amountOfMines);
        } catch (MinesweeperException e) {
            fieldIsInitialized = false;
        }*/
        this.view = view;
        view.setController(this);
        initializedGame(height, width, amountOfMines);
        /*if (model != null) {
            view.update(model.getField());
        }*/
        view.start();
    }

    private void leftClickHandle(int curCell) {
        if (isFirstClick) {
            model.placeAllMines(curCell);
            isFirstClick = false;
        }
        if (model.isCellMined(curCell) && (model.getCellState(curCell) != Cell.CellState.FLAG) && (model.getCellState(curCell) != Cell.CellState.QUESTION)) {
            isGameOver = true;
            model.openCell(curCell);
        } else {
            if (model.getCellState(curCell) == Cell.CellState.CLOSED) {
                model.openCell(curCell);
            }
        }
    }

    private void rightClickHandle(int curCell) {
        switch (model.getCellState(curCell)) {
            case FLAG:
                model.changeCellState(curCell, Cell.CellState.QUESTION);
                break;
            case CLOSED:
                model.changeCellState(curCell, Cell.CellState.FLAG);
                break;
            case QUESTION:
                model.changeCellState(curCell, Cell.CellState.CLOSED);
                break;
            case OPENED:
                default: break;
        }
    }

    private void doubleLeftClickHandle(int curCell) {
        if (!model.tryToOpenNeighbours(curCell)){
            isGameOver = true;
        }
    }

    private void clickCell(Click click, int curCell) {
            if (fieldIsInitialized) {
                switch (click) {
                    case LEFT:
                        leftClickHandle(curCell);
                        break;
                    case RIGHT:
                        rightClickHandle(curCell);
                        break;
                    case DOUBLE_LEFT:
                        doubleLeftClickHandle(curCell);
                        break;
                }
                if (model.isGameWon()) {
                    isGameWon = true;
                    //gameWasWon = true;
                }
            }
            view.update(model.getField());
    }

    public void clickCell(Click click, int row, int column) throws TooBigValueException {
        if(row >= model.getHeight() || column >= model.getWidth()) {
            throw new TooBigValueException("Too big value.");
        }
        clickCell(click, model.getField()[0].length * row + column);
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void startNewGameWithOldParameters(){
        startNewGame(model.getHeight(), model.getWidth(), model.getMinesInPercent());
    }

    public void startNewGame(int height, int width, int amountOfMines){
        gameRestarted = true;
        initializedGame(height, width, amountOfMines);
        gameRestarted = false;
    }

    private void initializedGame(int height, int width, int amountOfMines){
        isGameOver = false;
        isGameWon = false;
        isFirstClick = true;
        try {
            fieldIsInitialized = true;
            model = new Model(height, width, amountOfMines);
        } catch (MinesweeperException e) {
            fieldIsInitialized = false;
        }
        view.update(model.getField());
        timer.cancel();
        timer = new Timer();
        timer.scheduleAtFixedRate( //while !gameIsWon
                new TimerTask()
                {
                    int curSecond = 0;
                    public void run()
                    {
                        view.updateTime(curSecond);
                        curSecond++;
                    }
                },
                0,
                1000);
    }

    public void startNewGame(int height, int width, int amountOfMines, GameLevel gameLevel){
        this.gameLevel = gameLevel;
        startNewGame(height, width, amountOfMines);
    }

    public boolean isGameWon() {
        return isGameWon;
    }

    public int getHeight(){
        return model.getHeight();
    }

    public int getWidth(){
        return model.getWidth();
    }

    public int getMinesInPercents(){
        return model.getMinesInPercent();
    }

    public boolean isGameRestarted(){
        return gameRestarted;
    }

    //public boolean gameWasWon(){return gameWasWon;}
}