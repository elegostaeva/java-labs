package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class TooBigValueException extends MinesweeperException {
    public TooBigValueException(String message) {
        super(message);
    }
}
