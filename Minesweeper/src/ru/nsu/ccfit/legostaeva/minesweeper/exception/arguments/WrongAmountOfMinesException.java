package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class WrongAmountOfMinesException extends MinesweeperException {
    public WrongAmountOfMinesException(String message) {
        super(message);
    }
}
