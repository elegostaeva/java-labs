package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class TooBigFieldSize extends MinesweeperException {
    public TooBigFieldSize(String message) {
        super(message);
    }
}
