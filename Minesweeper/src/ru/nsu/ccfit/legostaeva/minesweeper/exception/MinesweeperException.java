package ru.nsu.ccfit.legostaeva.minesweeper.exception;

public class MinesweeperException extends Exception {
    public MinesweeperException(String message){super(message);}
}
