package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class NegativeValueOfArgumentException extends MinesweeperException {
    public NegativeValueOfArgumentException(String message) {
        super(message);
    }
}
