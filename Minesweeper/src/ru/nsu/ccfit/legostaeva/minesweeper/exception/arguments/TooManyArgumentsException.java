package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class TooManyArgumentsException extends MinesweeperException {
    public TooManyArgumentsException(String message) {
        super(message);
    }
}
