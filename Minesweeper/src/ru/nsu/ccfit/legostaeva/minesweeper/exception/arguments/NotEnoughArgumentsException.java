package ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments;

import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;

public class NotEnoughArgumentsException extends MinesweeperException {
    public NotEnoughArgumentsException(String message) {
        super(message);
    }
}
