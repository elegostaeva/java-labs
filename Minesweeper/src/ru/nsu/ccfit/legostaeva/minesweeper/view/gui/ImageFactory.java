package ru.nsu.ccfit.legostaeva.minesweeper.view.gui;

import javax.swing.*;
import java.awt.*;

class ImageFactory {
    private final static ImageFactory ourInstance = new ImageFactory();

    static ImageFactory getInstance() {
        return ourInstance;
    }

    private ImageFactory() {
    }

    Image getImage(String name){
        String filename = "/img/" + name.toLowerCase() + ".png";
        ImageIcon icon = new ImageIcon(getClass().getResource(filename));
        return icon.getImage();
    }
}
