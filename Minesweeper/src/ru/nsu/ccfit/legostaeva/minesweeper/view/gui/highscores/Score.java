package ru.nsu.ccfit.legostaeva.minesweeper.view.gui.highscores;

import java.io.Serializable;

public class Score  implements Serializable {
    private int score;
    private String naam;

    int getScore() {
        return score;
    }

    String getNaam() {
        return naam;
    }

    public Score(String naam, int score) {
        this.score = score;
        this.naam = naam;
    }
}

