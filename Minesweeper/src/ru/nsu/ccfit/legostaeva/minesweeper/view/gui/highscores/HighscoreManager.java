package ru.nsu.ccfit.legostaeva.minesweeper.view.gui.highscores;

import ru.nsu.ccfit.legostaeva.minesweeper.view.gui.GameLevel;

import java.util.*;
import java.io.*;

public class HighscoreManager {
    private static int max = 10;
    // An arraylist of the type "score" we will use to work with the scores inside the class
//    private ArrayList<Score> beginnerScores;
//    private ArrayList<Score> ntermediateScores;
//    private ArrayList<Score> scores;

    private HashMap<GameLevel, ArrayList<Score>> scores;
    // The name of the file where the highscores will be saved
    private static HashMap<GameLevel, String> filenames = new HashMap<>();

    private static final String BEGINNER_HIGHSCORE_FILE = "./resources/scores/beginner_scores.dat";
    private static final String INTERMEDIATE_HIGHSCORE_FILE = "./resources/scores/intermediate_scores.dat";
    private static final String EXPERT_HIGHSCORE_FILE = "./resources/scores/expert_scores.dat";

    static {
        filenames.put(GameLevel.BEGINNER, BEGINNER_HIGHSCORE_FILE);
        filenames.put(GameLevel.INTERMEDIATE, INTERMEDIATE_HIGHSCORE_FILE);
        filenames.put(GameLevel.EXPERT, EXPERT_HIGHSCORE_FILE);
    }

    //Initialising an in and outputStream for working with the file
    private ObjectOutputStream outputStream = null;
    private ObjectInputStream inputStream = null;

    public HighscoreManager() {
        //initialising the scores-arraylist
        scores = new HashMap<>();
        for (GameLevel gameLevel : GameLevel.values()) {
            if (gameLevel != GameLevel.SPECIAL) scores.put(gameLevel, new ArrayList<>());
        }
    }

    private ArrayList<Score> getScores(GameLevel gameLevel) {
        loadScoreFile();
        sort(gameLevel);
        return scores.get(gameLevel);
    }


    private void sort(GameLevel gameLevel) {
        ScoreComparator comparator = new ScoreComparator();
        scores.get(gameLevel).sort(comparator);
    }

    public void addScore(String name, int score, GameLevel gameLevel) {
        loadScoreFile();
        scores.get(gameLevel).add(new Score(name, score));
        updateScoreFile();
    }

    private void loadScoreFile() {
        for (GameLevel gameLevel : GameLevel.values()) {
            if (gameLevel != GameLevel.SPECIAL) {
                try {
                    inputStream = new ObjectInputStream(new FileInputStream(filenames.get(gameLevel)));
                    scores.put(gameLevel, (ArrayList<Score>) inputStream.readObject());
                } catch (FileNotFoundException e) {
                    System.out.println("[Laad] FNF Error: " + e.getMessage());
                } catch (IOException e) {
                    System.out.println("[Laad] IO Error: " + e.getMessage());
                } catch (ClassNotFoundException e) {
                    System.out.println("[Laad] CNF Error: " + e.getMessage());
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        System.out.println("[Laad] IO Error: " + e.getMessage());
                    }
                }
            }
        }
    }

    private void updateScoreFile() {
        for (GameLevel gameLevel : GameLevel.values()) {
            if (gameLevel != GameLevel.SPECIAL) {
                try {
                    outputStream = new ObjectOutputStream(new FileOutputStream(filenames.get(gameLevel)));
                    outputStream.writeObject(scores.get(gameLevel));
                } catch (FileNotFoundException e) {
                    System.out.println("[Update] FNF Error: " + e.getMessage() + ",the program will try and make a new file");
                } catch (IOException e) {
                    System.out.println("[Update] IO Error: " + e.getMessage());
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        System.out.println("[Update] Error: " + e.getMessage());
                    }
                }
            }
        }
    }

    public String getHighscoreString(GameLevel gameLevel) {
        StringBuilder highscoreString = new StringBuilder();

        ArrayList<Score> scores;
        scores = getScores(gameLevel);

        int i = 0;
        int x = scores.size();
        if (x > max) {
            x = max;
        }

        while (i < x) {
            highscoreString.append(i + 1).append(".  ").append(scores.get(i).getNaam()).append("    ").append(scores.get(i).getScore()).append("\n");
            i++;
        }

        return highscoreString.toString();
    }
}
