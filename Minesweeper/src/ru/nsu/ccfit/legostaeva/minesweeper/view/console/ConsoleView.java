package ru.nsu.ccfit.legostaeva.minesweeper.view.console;

import ru.nsu.ccfit.legostaeva.minesweeper.*;
import ru.nsu.ccfit.legostaeva.minesweeper.controller.Controller;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.MinesweeperException;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments.*;
import ru.nsu.ccfit.legostaeva.minesweeper.view.View;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class ConsoleView implements View {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[37m";
    private final PrintStream out;
    private Controller controller;
    private static final String newGame = "NEWGAME";
    private static final String exit = "EXIT";
    private static final String about = "ABOUT";


    public ConsoleView(PrintStream out) {
        this.out = out;
    }
    public ConsoleView() {
        this.out = System.out;
    }


    @Override
    public void update(Cell[][] field) {
        if (controller.isGameOver()) {
            out.println("Game is over. Do you want to start a new game? YES/NO: ");
            endOfGame();
        } else if (controller.isGameWon()) {
            out.println("Good job, you won! Do you want to start again? YES/NO: ");
            endOfGame();
        } else if (!controller.fieldIsInitialized) {
            out.println("Wrong number of mines.");
        } else {
            drawField(field);
        }
    }

    private void endOfGame() {
        Scanner in = new Scanner(System.in);
        String answer = in.nextLine();
        if (answer.toUpperCase().equals("YES")) {
            controller.startNewGameWithOldParameters();
        } else {
            System.exit(0);
        }
    }


    private void drawField(Cell[][] field) {
        out.print("    ");
        for (int i = 0; i < field[0].length; i++) {
            printCell(ANSI_CYAN, i);
        }
        out.println();

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; ++j) {
                if (j == 0) {
                    printCell(ANSI_CYAN, i);
                }
                switch (field[i][j].getCurrentState()) {
                    case OPENED:
                        printCell(field[i][j]);

                        break;
                    case QUESTION:
                        out.print(String.format("%1$4s", '?'));
                        break;
                    case FLAG:
                        out.print(ANSI_RED + String.format("%1$4s", 'P') + ANSI_RESET);
                        break;
                    case CLOSED:
                        out.print(String.format("%1$4s", '-'));
                }
            }
            out.println();
        }
    }

    private void printCell(Cell cell) {
        int minesAround = cell.getNumberOfMinesAround();
        String color;
        switch (minesAround) {
            case 0:
                color = ANSI_WHITE;
                break;
            case 1:
                color = ANSI_BLUE;
                break;
            case 2:
                color = ANSI_GREEN;
                break;
            case 3:
                color = ANSI_PURPLE;
                break;
            case 4:
                color = ANSI_YELLOW;
                break;
            case 5:
                color = ANSI_GREEN;
                break;
            case 6:
                color = ANSI_CYAN;
                break;
            case 7:
                color = ANSI_BLUE;
                break;
            default:
                color = ANSI_RED;
                break;
        }
        printCell(color, minesAround);

    }

    private void printCell(String color, int numberOfMinesAround) {
        out.print(color + String.format("%1$4s", numberOfMinesAround) + ANSI_RESET);
    }

    public void start() {
        CustomStreamReader customStreamReader = new CustomStreamReader(System.in);
        while (customStreamReader.hasNextLine()) {
            String curLine = customStreamReader.nextLine();
            List<String> params = Parser.getParams(curLine);
            if (params.size() == 0) {
                continue;
            }
            switch (params.get(0).toUpperCase()) {
                case newGame:
                    newGameHandler(params);
                    break;
                case exit:
                    if (exitHandler(customStreamReader)) return;
                case about:
                    aboutHandler();
                    break;
                default:
                    defaultHandler(params);
                    break;
            }
        }
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void updateTime(int seconds) {

    }

    private void newGameHandler(List<String> params) {
        try {
            if (params.size() < 4) {
                throw new NotEnoughArgumentsException("Not enough arguments.");
            }
            if (params.size() > 4) {
                throw new TooManyArgumentsException("Too many arguments.");
            }
            if (Integer.parseInt(params.get(1)) < 1 || Integer.parseInt(params.get(2)) < 1) {
                throw new NegativeValueOfArgumentException("Height, width and mines in percents can't be negative or zero.");
            }
            if (Integer.parseInt(params.get(1)) > 35 || Integer.parseInt(params.get(2)) > 35) {
                throw new TooBigFieldSize("Too big field.");
            }
            controller.startNewGame(Integer.parseInt(params.get(1)), Integer.parseInt(params.get(2)), Integer.parseInt(params.get(3)));
        } catch (MinesweeperException e) {
            out.println(e.getMessage());
        } catch (NumberFormatException e) {
            out.println("Height and width must be integer.");
        }
    }

    private boolean exitHandler(CustomStreamReader customStreamReader) {
        out.println("Are you sure you want to end the game? YES/NO: ");
        String answer = customStreamReader.nextLine();
        return (answer.toUpperCase().equals("YES"));
    }

    private void defaultHandler(List<String> params) {
        try {
            Click click = Factory.getInstance().getClickTypeByName(params.get(0));
            if (params.size() < 3) {
                throw new NotEnoughArgumentsException("Not enough arguments.");
            }
            if (params.size() > 3) {
                throw new TooManyArgumentsException("Too many arguments.");
            }
            try {
                if (Integer.parseInt(params.get(1)) < 0 || Integer.parseInt(params.get(2)) < 0) {
                    throw new NegativeValueOfArgumentException("Cell's arguments can't be negative.");
                }
                controller.clickCell(click, Integer.parseInt(params.get(1)), Integer.parseInt(params.get(2)));
            } catch (TooBigValueException e) {
                out.println(e.getMessage());
            } catch (NumberFormatException e) {
                out.println("Cell's arguments must be integer.");
            }
        } catch (MinesweeperException | UnsupportedOperationException e) {
            out.println(e.getMessage());
        }
    }

    private void aboutHandler(){
        try (BufferedReader in = new BufferedReader(new FileReader("./resources/about.txt"))) {
            String line;
            while ((line = in.readLine()) != null) {
                out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
