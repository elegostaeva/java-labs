package ru.nsu.ccfit.legostaeva.minesweeper.view.gui;

public enum GameLevel {
    BEGINNER,
    INTERMEDIATE,
    EXPERT,
    SPECIAL
}
