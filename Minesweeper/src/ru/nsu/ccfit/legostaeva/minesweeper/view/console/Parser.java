package ru.nsu.ccfit.legostaeva.minesweeper.view.console;

import java.util.ArrayList;
import java.util.Collections;

class Parser {
    static ArrayList<String> getParams(String curString){
        ArrayList<String> result = new ArrayList<>();

        if (curString.isEmpty()){
            return result;
        }

        Collections.addAll(result, curString.split(" +"));

        return result;
    }
}
