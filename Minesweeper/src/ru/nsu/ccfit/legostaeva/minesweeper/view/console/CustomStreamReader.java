package ru.nsu.ccfit.legostaeva.minesweeper.view.console;

import java.io.InputStream;
import java.util.Scanner;

class CustomStreamReader{

    private Scanner reader;

    CustomStreamReader(InputStream stream){
        this.reader = new Scanner(stream);
    }

    String nextLine(){
        return reader.nextLine();
    }

    boolean hasNextLine() {
        return reader.hasNextLine();
    }
}
