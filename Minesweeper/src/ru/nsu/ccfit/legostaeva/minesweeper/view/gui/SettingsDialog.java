package ru.nsu.ccfit.legostaeva.minesweeper.view.gui;

import ru.nsu.ccfit.legostaeva.minesweeper.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

class SettingsDialog extends JDialog {
    private Map<String, JComponent> map = new HashMap<>();
    private GameLevel gameLevel;

    private void setGameLevel(GameLevel gameLevel) {
        this.gameLevel = gameLevel;
    }

    SettingsDialog(Frame owner, String title, int height, int width, int numberOfMinesInPercents, GameLevel gameLevel, Controller controller) {
        super(owner, title);
        setLayout(new BorderLayout());
        JPanel settingsPanel = new JPanel();
        this.gameLevel = gameLevel;
        JRadioButton beginner = new JRadioButton("BEGINNER");
        JRadioButton intermediate = new JRadioButton("INTERMEDIATE");
        JRadioButton expert = new JRadioButton("EXPERT");
        JRadioButton special = new JRadioButton("SPECIAL");
        ButtonGroup settingsGroup = new ButtonGroup();
        settingsGroup.add(beginner);
        settingsGroup.add(intermediate);
        settingsGroup.add(expert);
        settingsGroup.add(special);



        String[] labels = {"Height: ", "Width: ", "Mines in percents: "};
        int numPairs = labels.length;
        JPanel p = new JPanel(new SpringLayout());
        for (String label : labels) {
            JLabel l = new JLabel(label, JLabel.TRAILING);
            p.add(l);
            JTextField textField = new JTextField(10);
            l.setLabelFor(textField);
            p.add(textField);
            if (label.equals("Height: ")) {
                textField.setText(Integer.toString(height));
                map.put("height", textField);
            } else if (label.equals("Width: ")) {
                textField.setText(Integer.toString(width));
                map.put("width", textField);
            } else {
                textField.setText(Integer.toString(numberOfMinesInPercents));
                map.put("mines", textField);
            }

        }
        makeCompactGrid(p,
                numPairs, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        settingsPanel.setLayout(new GridLayout(4, 1));
        settingsPanel.add(beginner);
        settingsPanel.add(intermediate);
        settingsPanel.add(expert);
        settingsPanel.add(special);
        add(settingsPanel, BorderLayout.NORTH);
        add(p, BorderLayout.CENTER);
        JPanel okPanel = new JPanel();
        JButton okButton = new JButton("OK");
        okPanel.add(okButton);
        add(okPanel, BorderLayout.SOUTH);

        settingsPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Level"));

        p.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Values"));

        //ВЫБОР ТОЧКИ
        if (gameLevel == GameLevel.BEGINNER) {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            beginner.setSelected(true);
        } else if (gameLevel == GameLevel.INTERMEDIATE) {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            intermediate.setSelected(true);
        } else if (gameLevel == GameLevel.EXPERT){
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            expert.setSelected(true);
        }else {
            special.setSelected(true);
        }

        beginner.addActionListener(e -> {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            setTextFieldValues(9, 9, 20);
            setGameLevel(GameLevel.BEGINNER);
        });
        intermediate.addActionListener(e -> {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            setTextFieldValues(16, 16, 30);
            setGameLevel(GameLevel.INTERMEDIATE);
        });
        expert.addActionListener(e -> {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(false);
            }
            setTextFieldValues(30, 16, 40);
            setGameLevel(GameLevel.EXPERT);
        });
        special.addActionListener(e -> {
            Component[] com = p.getComponents();
            for (Component component : com) {
                component.setEnabled(true);
            }
            setGameLevel(GameLevel.SPECIAL);
        });
        special.addActionListener(e -> p.setEnabled(true));

        okButton.addActionListener(e -> {
            int curHeight = Integer.parseInt(((JTextField)map.get("height")).getText()); //TODO convert method
            int curWidth = Integer.parseInt(((JTextField)map.get("width")).getText());
            int curMines = Integer.parseInt(((JTextField)map.get("mines")).getText());
            controller.startNewGame(curHeight, curWidth, curMines, this.gameLevel);
            dispose();
        });

        pack();
        if (owner != null) {
            setLocationRelativeTo(owner); //по центру
        }
        setModal(true); //чтобы не нажимать назад
        setResizable(false);
        setVisible(true);
    }


    private static SpringLayout.Constraints getConstraintsForCell(
            int row, int col,
            Container parent,
            int cols) {
        SpringLayout layout = (SpringLayout) parent.getLayout();
        Component c = parent.getComponent(row * cols + col);
        return layout.getConstraints(c);
    }

    private static void makeCompactGrid(Container parent,
                                        int rows, int cols,
                                        int initialX, int initialY,
                                        int xPad, int yPad) {
        SpringLayout layout;
        try {
            layout = (SpringLayout) parent.getLayout();
        } catch (ClassCastException exc) {
            System.err.println("The first argument to makeCompactGrid must use SpringLayout.");
            return;
        }

        //Align all cells in each column and make them the same width.
        Spring x = Spring.constant(initialX);
        for (int c = 0; c < cols; c++) {
            Spring width = Spring.constant(0);
            for (int r = 0; r < rows; r++) {
                width = Spring.max(width,
                        getConstraintsForCell(r, c, parent, cols).
                                getWidth());
            }
            for (int r = 0; r < rows; r++) {
                SpringLayout.Constraints constraints =
                        getConstraintsForCell(r, c, parent, cols);
                constraints.setX(x);
                constraints.setWidth(width);
            }
            x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
        }

        //Align all cells in each row and make them the same height.
        Spring y = Spring.constant(initialY);
        for (int r = 0; r < rows; r++) {
            Spring height = Spring.constant(0);
            for (int c = 0; c < cols; c++) {
                height = Spring.max(height,
                        getConstraintsForCell(r, c, parent, cols).
                                getHeight());
            }
            for (int c = 0; c < cols; c++) {
                SpringLayout.Constraints constraints =
                        getConstraintsForCell(r, c, parent, cols);
                constraints.setY(y);
                constraints.setHeight(height);
            }
            y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
        }

        //Set the parent's size.
        SpringLayout.Constraints pCons = layout.getConstraints(parent);
        pCons.setConstraint(SpringLayout.SOUTH, y);
        pCons.setConstraint(SpringLayout.EAST, x);
    }

    private void setTextFieldValues(int height, int width, int amountOfMinesInPercents){
        ((JTextField)map.get("height")).setText(Integer.toString(height));
        ((JTextField)map.get("width")).setText(Integer.toString(width));
        ((JTextField)map.get("mines")).setText(Integer.toString(amountOfMinesInPercents));
    }
}
