package ru.nsu.ccfit.legostaeva.minesweeper.view.console;

import ru.nsu.ccfit.legostaeva.minesweeper.Click;

class Factory {
    private static Factory ourInstance = new Factory();

    static Factory getInstance() {
        return ourInstance;
    }

    private Factory() {
    }

    Click getClickTypeByName(String clickName){
        switch (clickName.toUpperCase()){
            case "LEFT":
                return Click.LEFT;
            case "RIGHT":
                return Click.RIGHT;
            case "DLEFT":
                return Click.DOUBLE_LEFT;
            default:
                throw new UnsupportedOperationException("No such command.");
        }
    }
}
