package ru.nsu.ccfit.legostaeva.minesweeper.view.gui;

import ru.nsu.ccfit.legostaeva.minesweeper.Cell;
import ru.nsu.ccfit.legostaeva.minesweeper.Click;
import ru.nsu.ccfit.legostaeva.minesweeper.controller.Controller;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments.TooBigValueException;
import ru.nsu.ccfit.legostaeva.minesweeper.view.View;
import ru.nsu.ccfit.legostaeva.minesweeper.view.gui.highscores.HighscoreManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class GUIView extends JFrame implements View {
    private int curTime;
    private JMenuBar menuBar = new JMenuBar(); // Window menu bar
    private JLabel time = new JLabel();
    private final int IMAGE_SIZE = 50;
    private JPanel panel;
    private Controller controller;
    private Cell[][] field = null;
    private boolean gameIsOver = false;
    private GameLevel gameLevel = GameLevel.BEGINNER;
    private HighscoreManager hm = new HighscoreManager();


    public GUIView() {
    }

    @Override
    public void update(Cell[][] field) {
        this.field = field;
        this.gameLevel = controller.getGameLevel();
        if(controller.isGameRestarted()){
            panel.setPreferredSize(new Dimension(controller.getHeight() * IMAGE_SIZE, controller.getWidth() * IMAGE_SIZE));
            pack();
            invalidate();
        }
        if (controller.isGameOver()) {
            gameIsOver = true;
            if (panel != null) {
                panel.repaint();
            }
            int n = JOptionPane.showConfirmDialog(
                    this,
                    "Would you like to play again?",
                    "Game over",
                    JOptionPane.YES_NO_OPTION);
            endOfGame(n);
        } else if (controller.isGameWon()) {
            gameIsOver = true;
            if (panel != null) {
                panel.repaint();
            }
            if (gameLevel != GameLevel.SPECIAL) {
                String name = JOptionPane.showInputDialog(this, "Enter your name", "Game won", JOptionPane.INFORMATION_MESSAGE);
                hm.addScore(name, curTime, gameLevel);
            }
            int n = JOptionPane.showConfirmDialog(
                    this,
                    "Would you like to play again?",
                    "Game won",
                    JOptionPane.YES_NO_OPTION);
            endOfGame(n);

        } else {
            if (panel != null) {
                panel.repaint();
            }
        }
    }

    private void endOfGame(int chosenOption) {
        if (chosenOption == JOptionPane.YES_OPTION) {
            gameIsOver = false;
            controller.startNewGameWithOldParameters();
        } else {
            System.exit(0);
        }
    }

    @Override
    public void start() {
        initMenu();
        initPanel();
        initFrame();
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    private void initPanel() {
        panel = new JPanel() { //анонимный класс
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                if (field == null) {
                    return;
                }
                for (int i = 0; i < field.length; ++i) {
                    for (int j = 0; j < field[0].length; ++j) {
                        g.drawImage(getImageByCell(field[i][j]), i * IMAGE_SIZE, j * IMAGE_SIZE, this);
                    }
                }
            }
        };

        panel.addMouseListener(new MouseAdapter() {
                                   @Override
                                   public void mousePressed(MouseEvent e) {
                                       int x = e.getX() / IMAGE_SIZE;
                                       int y = e.getY() / IMAGE_SIZE;
                                       try {
                                           if ((e.getClickCount() == 2) && e.getButton() == MouseEvent.BUTTON1) {
                                               controller.clickCell(Click.DOUBLE_LEFT, x, y);
                                               return;
                                           }
                                           if (e.getButton() == MouseEvent.BUTTON1) // левая кнопка мыши
                                               controller.clickCell(Click.LEFT, x, y);
                                           if (e.getButton() == MouseEvent.BUTTON3) // правая кнока мыши
                                               controller.clickCell(Click.RIGHT, x, y);
                                       } catch (TooBigValueException ex) {
                                           ex.printStackTrace();
                                       }
                                   }
                               }
        );

        panel.setPreferredSize(new Dimension(controller.getHeight() * IMAGE_SIZE, controller.getWidth() * IMAGE_SIZE));
        add(panel);
    }

    private void initFrame() {
        pack(); //установит размер поля
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Minesweeper");
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        setIconImage(ImageFactory.getInstance().getImage("icon"));
    }

    private Image getImageByCell(Cell curCell) {
        ImageFactory imageFactory = ImageFactory.getInstance();
        if (!gameIsOver) {
            switch (curCell.getCurrentState()) {
                case OPENED:
                    if (curCell.isMined()) {
                        return imageFactory.getImage("bombed");
                    } else {
                        return imageFactory.getImage("num" + curCell.getNumberOfMinesAround());
                    }
                case QUESTION:
                    return imageFactory.getImage("question");
                case FLAG:
                    return imageFactory.getImage("flaged");
                case CLOSED:
                    return imageFactory.getImage("closed");
            }
        }else{
            switch (curCell.getCurrentState()){
                case CLOSED:
                if(curCell.isMined()){
                    return imageFactory.getImage("bomb");
                }else{
                    return imageFactory.getImage("closed");
                }
                case OPENED:
                if(curCell.isMined()){
                    return imageFactory.getImage("bombed");
                }else{
                    return imageFactory.getImage("num" + curCell.getNumberOfMinesAround());
                }
                case FLAG:
                if(curCell.isMined()){
                    return imageFactory.getImage("flaged");
                }else{
                    return imageFactory.getImage("nobomb");
                }
                case QUESTION:
                return imageFactory.getImage("question");
            }
        }
        return null;
    }

    private void initMenu() {
        setJMenuBar(menuBar); // Add the menu bar to the window
        JMenu gameMenu = new JMenu("Game"); // Create newGame
        menuBar.add(gameMenu); // Add the file menu
        JMenuItem newGame = new JMenuItem("Start new game");
        gameMenu.add(newGame);
        newGame.addActionListener(e -> controller.startNewGameWithOldParameters());
        JMenuItem settings = new JMenuItem("Settings");
        gameMenu.add(settings);
        settings.addActionListener(e -> new SettingsDialog(this, "Settings", controller.getHeight(), controller.getWidth(), controller.getMinesInPercents(), gameLevel, controller));
        JMenuItem exit = new JMenuItem("Exit");
        gameMenu.add(exit);
        exit.addActionListener(e -> System.exit(0));
        JMenu highScores = new JMenu("High Scores");
        menuBar.add(highScores);
        JMenuItem beginner = new JMenuItem("Beginner");
        highScores.add(beginner);
        beginner.addActionListener(e -> JOptionPane.showMessageDialog(GUIView.this, hm.getHighscoreString(GameLevel.BEGINNER)));
        JMenuItem intermediate = new JMenuItem("Intermediate");
        highScores.add(intermediate);
        intermediate.addActionListener(e -> JOptionPane.showMessageDialog(GUIView.this, hm.getHighscoreString(GameLevel.INTERMEDIATE)));
        JMenuItem expert = new JMenuItem("Expert");
        highScores.add(expert);
        expert.addActionListener(e -> JOptionPane.showMessageDialog(GUIView.this, hm.getHighscoreString(GameLevel.EXPERT)));
        //highScores.addActionListener(e -> JOptionPane.showMessageDialog(GUIView.this, hm.getHighscoreString()));
        JMenu about = new JMenu("About");
        menuBar.add(about);
        JMenuItem help = new JMenuItem("Help");
        about.add(help);
        help.addActionListener(e -> JOptionPane.showMessageDialog(GUIView.this,
                getHelpMessage()));
        menuBar.add(time);
    }

    private String getHelpMessage(){
        StringBuilder help = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader("./resources/about.txt"))) {
            String line;
            while ((line = in.readLine()) != null) {
                help.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return help.toString();
    }

    public void updateTime(int seconds){
        if (!controller.isGameWon() && !controller.isGameOver()){
            time.setText("Time: " + seconds);
            curTime = seconds;
        }
    }
}
