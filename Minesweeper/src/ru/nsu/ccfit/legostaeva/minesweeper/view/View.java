package ru.nsu.ccfit.legostaeva.minesweeper.view;

import ru.nsu.ccfit.legostaeva.minesweeper.Cell;
import ru.nsu.ccfit.legostaeva.minesweeper.controller.Controller;

public interface View {
    void update(Cell[][] field);
    void start();
    void setController(Controller controller);
    void updateTime(int seconds);
}
