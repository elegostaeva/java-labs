package ru.nsu.ccfit.legostaeva.minesweeper;

import ru.nsu.ccfit.legostaeva.minesweeper.controller.Controller;
import ru.nsu.ccfit.legostaeva.minesweeper.view.console.ConsoleView;
import ru.nsu.ccfit.legostaeva.minesweeper.view.gui.GUIView;


public class Main{
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    public static void main(String[] args){
        if(args.length > 0 && "console".equals(args[0])) {
            System.out.println(ANSI_RED + "MINESWEEPER" + ANSI_RESET);
            System.out.println("This game starts with the standard field 9x9 and with 20% of mines. If don't know how to play, please, use command 'ABOUT'. Good luck!");
            new Controller(9, 9, 20, new ConsoleView());
        }else{
            new Controller(9, 9, 20, new GUIView());
        }
    }
}