package ru.nsu.ccfit.legostaeva.minesweeper.model;

import ru.nsu.ccfit.legostaeva.minesweeper.Cell;
import ru.nsu.ccfit.legostaeva.minesweeper.exception.arguments.WrongAmountOfMinesException;

import java.util.Random;

import static ru.nsu.ccfit.legostaeva.minesweeper.Cell.CellState.*;

public class Model {
    private Cell[][] field;
    private int width;
    private int height;
    private int amountOfMines;
    private int minesInPercent;
    private boolean minesArePlaced = false;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMinesInPercent() {
        return minesInPercent;
    }

    public int getAmountOfMines() {
        return amountOfMines;
    }

    public Model(int height, int width, int amountOfMines) throws WrongAmountOfMinesException{
        minesInPercent = amountOfMines;
        if(amountOfMines < 5 || amountOfMines > 90){
            throw new WrongAmountOfMinesException("Wrong amount of mines.");
        }
        this.width = width;
        this.height = height;
        this.amountOfMines = amountOfMines * width * height / 100;
        field = new Cell[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                field[i][j] = new Cell();
            }
        }
    }

    /* we can't put mine in curCell, because it's first clicked cell*/
    public void placeAllMines(int curCell) {
        int[] tempArray = new int[width * height];
        for (int i = 0; i < tempArray.length; ++i) {
            tempArray[i] = i;
        }
        Random random = new Random();
        tempArray[0] = curCell;
        tempArray[curCell] = 0;
        for (int i = 1; i < amountOfMines + 1; ++i) {
            int tmpMine = random.nextInt(tempArray.length - i) + i;
            int tmp = tempArray[i];
            tempArray[i] = tempArray[tmpMine];
            tempArray[tmpMine] = tmp;
            //System.out.println(tempArray[i]);
            field[tempArray[i] / width][tempArray[i] % width].setMined(true);
        }
        calculateField();
        minesArePlaced = true;
    }

    private void calculateField() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (!field[i][j].isMined()) continue;
                if ((i - 1 >= 0) && (j - 1 >= 0)) {
                    field[i - 1][j - 1].increaseNumberOfMinesAround();
                }
                if ((i - 1 >= 0)) {
                    field[i - 1][j].increaseNumberOfMinesAround();
                }
                if ((i - 1 >= 0) && (j + 1 < width)) {
                    field[i - 1][j + 1].increaseNumberOfMinesAround();
                }
                if ((j - 1 >= 0)) {
                    field[i][j - 1].increaseNumberOfMinesAround();
                }
                if ((j + 1 < width)) {
                    field[i][j + 1].increaseNumberOfMinesAround();
                }
                if ((i + 1 < height) && (j - 1 >= 0)) {
                    field[i + 1][j - 1].increaseNumberOfMinesAround();
                }
                if ((i + 1 < height)) {
                    field[i + 1][j].increaseNumberOfMinesAround();
                }
                if ((i + 1 < height) && (j + 1 < width)) {
                    field[i + 1][j + 1].increaseNumberOfMinesAround();
                }
            }
        }
    }


    public boolean isCellMined(int curCell) {
        return field[curCell / width][curCell % width].isMined();
    }

    private void openCell(int row, int column) {
        if (row < 0 || row >= height || column < 0 || column >= width || field[row][column].getCurrentState() == Cell.CellState.OPENED) {
            return;
        }
        field[row][column].setCurrentState(Cell.CellState.OPENED);
        if(field[row][column].isMined()){
            return;
        }
        if (field[row][column].getNumberOfMinesAround() == 0) {
            for (int i = row - 1; i <= row + 1; ++i) {
                for (int j = column - 1; j <= column + 1; ++j) {
                    if (i != row || j != column) {
                        openCell(i, j);
                    }
                }
            }

        }
    }

    public void openCell(int curCell) {
        openCell(curCell / width, curCell % width);
    }

    public Cell.CellState getCellState(int curCell) {
        return field[curCell / width][curCell % width].getCurrentState();
    }

    public void changeCellState(int curCell, Cell.CellState newState) {
        field[curCell / width][curCell % width].setCurrentState(newState);
    }

    public Cell[][] getField() {
        return field;
    }

    public boolean tryToOpenNeighbours(int curCell){
        int row = curCell / width;
        int column = curCell % width;
        if(field[row][column].getCurrentState() != OPENED){
            return true;
        }
        for (int i = row - 1; i <= row + 1; ++i) {
            for (int j = column - 1; j <= column + 1; ++j) {
                if (i < 0 || i >= height || j < 0 || j >= width){
                    continue;
                }
                if (field[i][j].getCurrentState() == FLAG && !field[i][j].isMined() ||
                field[i][j].getCurrentState() != FLAG && field[i][j].isMined()){
                    return false;
                }
                else {
                    if (!field[i][j].isMined()) {
                        openCell(i, j);
                    }
                }
            }
        }
        return true;
    }

    public boolean isGameWon(){
        return minesArePlaced && (areAllMinesFlag() || areAllUnminedCellsOpened());
    }

    private boolean areAllMinesFlag(){
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                if((field[i][j].isMined() && (field[i][j].getCurrentState() != FLAG)) || (!field[i][j].isMined() && (field[i][j].getCurrentState() == FLAG))){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean areAllUnminedCellsOpened(){
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                if(!field[i][j].isMined() && field[i][j].getCurrentState() != OPENED){
                    return false;
                }
            }
        }
        return true;
    }
}
